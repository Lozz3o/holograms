package dev.lozz.arcanegames.holograms;

import dev.lozz.arcanegames.holograms.commands.HologramCommand;
import dev.lozz.arcanegames.holograms.holograms.HologramHandler;
import dev.lozz.arcanegames.holograms.listeners.HologramListener;
import dev.lozz.arcanegames.holograms.utils.CommandUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class ArcaneHolograms extends JavaPlugin {

    private static ArcaneHolograms instance;

    private HologramHandler hologramHandler;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        instance = this;
        hologramHandler = new HologramHandler();

        Bukkit.getPluginManager().registerEvents(new HologramListener(), this);
        CommandUtils.registerCommand(new HologramCommand());
    }
    @Override
    public void onDisable() {
        hologramHandler.save();
    }
    public static ArcaneHolograms getInstance() {
        return instance;
    }
    public HologramHandler getHologramHandler() {
        return hologramHandler;
    }
}
