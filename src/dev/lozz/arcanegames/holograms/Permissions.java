package dev.lozz.arcanegames.holograms;

public class Permissions {

    public static final String HOLOGRAM_COMMAND = "arcanegames.holograms.command";
    public static final String HOLOGRAM_COMMAND_CREATE = "arcanegames.holograms.command.create";
    public static final String HOLOGRAM_COMMAND_DELETE = "arcanegames.holograms.command.delete";
    public static final String HOLOGRAM_COMMAND_MOVE = "arcanegames.holograms.command.move";
    public static final String HOLOGRAM_COMMAND_LIST = "arcanegames.holograms.command.list";
    public static final String HOLOGRAM_COMMAND_ADDLINE = "arcanegames.holograms.command.addline";
    public static final String HOLOGRAM_COMMAND_REMOVELINE = "arcanegames.holograms.command.removeline";
    public static final String HOLOGRAM_COMMAND_SETLINE = "arcanegames.holograms.command.setline";
}
