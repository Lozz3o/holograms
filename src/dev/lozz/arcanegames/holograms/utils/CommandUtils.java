package dev.lozz.arcanegames.holograms.utils;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.defaults.BukkitCommand;

import java.lang.reflect.Field;

public class CommandUtils {

    private static CommandMap commandMap;

    static {
        try {
            Field field = Bukkit.getServer().getClass().getDeclaredField("commandMap");

            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            commandMap = (CommandMap) field.get(Bukkit.getServer());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void registerCommand(BukkitCommand command) {
        if (command.getName() == null) return;

        commandMap.register(command.getName(), command);
    }
}
