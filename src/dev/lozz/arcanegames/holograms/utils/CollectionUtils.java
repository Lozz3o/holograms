package dev.lozz.arcanegames.holograms.utils;

import java.util.Collection;
import java.util.List;

public class CollectionUtils {

    public static String combineCollection(Collection<String> collection) {
        String combined = "";
        int count = 1;

        for (String s : collection) {
            if (count++ == collection.size()) {
                combined += s;
                continue;
            }
            combined += s + "\n";
        }
        return combined;
    }
    public static <T> List<T> paginateList(List<T> list, int page, int linesPerPage) {
        int min = page <= 1 ? 0 : ((page - 1) * linesPerPage);
        int max = min + linesPerPage;

        max = max > list.size() ? list.size() : max;

        return list.subList(min, max);
    }
}
