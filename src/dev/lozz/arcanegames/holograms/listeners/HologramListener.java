package dev.lozz.arcanegames.holograms.listeners;

import org.bukkit.entity.ArmorStand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;

public class HologramListener implements Listener {

    @EventHandler
    public void onInteract(PlayerArmorStandManipulateEvent event) {
        ArmorStand entity = event.getRightClicked();

        if (entity.isVisible()) return;
        if (entity.getCanPickupItems()) return;
        if (entity.hasGravity()) return;

        event.setCancelled(true);
    }
}
