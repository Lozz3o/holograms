package dev.lozz.arcanegames.holograms.holograms;

import dev.lozz.arcanegames.holograms.utils.ColorUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Hologram {

    private String name;
    private List<ArmorStand> entities;
    private boolean spawned;
    private List<String> lines;
    private Location location;

    private static final double distance = 0.25;

    protected Hologram(String name, Location location, List<String> lines) {
        this.name = name;
        entities = new ArrayList();
        this.lines = new ArrayList<>();
        spawned = false;
        this.location = location;
        this.lines = lines;
    }
    public String getName() {
        return name;
    }
    public Location getLocation() {
        return location;
    }
    public List<String> getLines() {
        return lines;
    }
    protected void spawn() {
        if (spawned) return;

        World world = location.getWorld();
        int count = 1;

        for (String line : lines) {
            ArmorStand entity = (ArmorStand) world.spawnEntity(location.clone().subtract(0, count++ * distance, 0), EntityType.ARMOR_STAND);

            entity.setGravity(false);
            entity.setCanPickupItems(false);
            entity.setVisible(false);
            entity.setCustomNameVisible(true);
            entity.setCustomName(ColorUtils.translateMessage(line));

            entities.add(entity);
        }
        spawned = true;
    }
    protected void despawn() {
        if (!spawned) return;

        for (ArmorStand entity : entities) {
            entity.remove();
        }
        entities.clear();

        spawned = false;
    }
    protected void addLine(String line) {
        lines.add(line);

        World world = location.getWorld();

        ArmorStand entity = (ArmorStand) world.spawnEntity(location.clone().subtract(0, lines.size() * distance, 0), EntityType.ARMOR_STAND);

        entity.setGravity(false);
        entity.setCanPickupItems(false);
        entity.setVisible(false);
        entity.setCustomNameVisible(true);
        entity.setCustomName(ColorUtils.translateMessage(line));

        entities.add(entity);
    }
    protected void removeLine(int line) {
        lines.remove(line);

        despawn();
        spawn();
    }
    protected void setLine(int line, String text) {
        lines.set(line, text);

        despawn();
        spawn();
    }
    protected void move(Location location) {
        this.location = location;

        despawn();
        spawn();
    }
}
