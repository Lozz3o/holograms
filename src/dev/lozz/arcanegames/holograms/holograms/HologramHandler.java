package dev.lozz.arcanegames.holograms.holograms;

import dev.lozz.arcanegames.holograms.utils.ConfigurationUtils;
import dev.lozz.arcanegames.holograms.utils.FileUtils;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class HologramHandler {

    private final Set<Hologram> holograms;

    public HologramHandler() {
        holograms = new HashSet<>();

        loadHolograms();
    }
    private void loadHolograms() {
        File directory = new File("plugins" + File.separator + "ArcaneHolograms" + File.separator + "Holograms");

        directory.mkdirs();

        for (File file : directory.listFiles()) {
            FileConfiguration config = YamlConfiguration.loadConfiguration(file);

            String name = file.getName();
            Location location = ConfigurationUtils.deserializeLocation(config, "Location");
            List<String> lines = config.getStringList("Lines");

            Hologram hologram = new Hologram(name, location, lines);

            holograms.add(hologram);

            hologram.spawn();
        }
    }
    private void saveHolograms() {
        File directory = new File("plugins" + File.separator + "ArcaneHolograms" + File.separator + "Holograms");

        directory.mkdirs();

        for (Hologram hologram : holograms) {

            File file = new File(directory, hologram.getName());

            if (!file.exists()) {
                FileUtils.createFile(file);
            }
            FileConfiguration config = YamlConfiguration.loadConfiguration(file);

            ConfigurationUtils.serializeLocation(config, hologram.getLocation(), "Location");
            config.set("Lines", hologram.getLines());

            FileUtils.saveFile(config, file);

            hologram.despawn();
        }
    }
    public void save() {
        saveHolograms();
    }
    public Hologram createHologram(String name, String line, Location location) {
        List<String> lines = new ArrayList<>();

        lines.add(line);

        Hologram hologram = new Hologram(name, location, lines);

        holograms.add(hologram);

        hologram.spawn();

        return hologram;
    }
    public Hologram getHologramByName(String name) {
        for (Hologram hologram : holograms) {
            if (!hologram.getName().equalsIgnoreCase(name)) continue;

            return hologram;
        }
        return null;
    }
    public void deleteHologram(Hologram hologram) {
        holograms.remove(hologram);
        hologram.despawn();
        deleteFileIfExists(hologram);
    }
    public void addLine(String line, Hologram hologram) {
        hologram.addLine(line);
    }
    private void deleteFileIfExists(Hologram hologram) {
        File directory = new File("plugins" + File.separator + "ArcaneHolograms" + File.separator + "Holograms");

        File file = new File(directory, hologram.getName());

        if (!file.exists()) return;

        FileUtils.deleteFile(file);
    }
    public void move(Hologram hologram, Location location) {
        hologram.move(location);
    }
    public List<Hologram> getHolograms() {
        return holograms.stream().collect(Collectors.toList());
    }
    public boolean removeLine(int line, Hologram hologram) {
        int size = hologram.getLines().size();
        line -= 1;

        if (line >= size) return false;

        if (size <= 1) {
            deleteHologram(hologram);
            return true;
        }
        hologram.removeLine(line);
        return true;
    }
    public boolean setLine(int line, Hologram hologram, String text) {
        int size = hologram.getLines().size();
        line -= 1;

        if (line >= size) return false;

        hologram.setLine(line, text);
        return true;
    }
}
