package dev.lozz.arcanegames.holograms.commands;

import dev.lozz.arcanegames.holograms.ArcaneHolograms;
import dev.lozz.arcanegames.holograms.Permissions;
import dev.lozz.arcanegames.holograms.holograms.Hologram;
import dev.lozz.arcanegames.holograms.holograms.HologramHandler;
import dev.lozz.arcanegames.holograms.utils.CollectionUtils;
import dev.lozz.arcanegames.holograms.utils.ColorUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class HologramCommand extends BukkitCommand {

    private final FileConfiguration config;
    private final String usage;
    private final HologramHandler hologramHandler;

    public HologramCommand() {
        super ("Hologram");

        config = ArcaneHolograms.getInstance().getConfig();

        usage = ColorUtils.translateMessage(CollectionUtils.combineCollection(config.getStringList("CommandUsage")).replace("{COMMAND}", getName()));
        hologramHandler = ArcaneHolograms.getInstance().getHologramHandler();
    }
    @Override
    public boolean execute(CommandSender sender, String label, String[] args) {
        if (!sender.hasPermission(Permissions.HOLOGRAM_COMMAND)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length < 2) {
            sender.sendMessage(usage);
            return false;
        }
        if (args[0].equalsIgnoreCase("create")) {

            return create(sender, args);

        } else if (args[0].equalsIgnoreCase("delete")) {

            return delete(sender, args);

        } else if (args[0].equalsIgnoreCase("move")) {

            return move(sender, args);

        } else if (args[0].equalsIgnoreCase("list")) {

            return list(sender, args);

        } else if (args[0].equalsIgnoreCase("addline")) {

            return addLine(sender, args);

        } else if (args[0].equalsIgnoreCase("removeline")) {

            return removeLine(sender, args);

        } else if (args[0].equalsIgnoreCase("setline")) {

            return setLine(sender, args);

        } else {
            sender.sendMessage(usage);
            return false;
        }
    }
    private boolean create(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("OnlyPlayers")));
            return false;
        }
        if (!sender.hasPermission(Permissions.HOLOGRAM_COMMAND_CREATE)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length < 3) {
            sender.sendMessage(usage);
            return false;
        }
        Hologram  hologram = hologramHandler.getHologramByName(args[1]);

        if (hologram != null) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("HologramAlreadyExists")));
            return false;
        }
        String text = "";

        for (int i = 2; i < args.length; i++) {
            text += text.isEmpty() ? args[i] : " " + args[i];
        }
        hologramHandler.createHologram(args[1], text, ((Player) sender).getLocation());
        sender.sendMessage(ColorUtils.translateMessage(config.getString("HologramCreated")));

        return true;
    }
    private boolean delete(CommandSender sender, String[] args) {
        if (!sender.hasPermission(Permissions.HOLOGRAM_COMMAND_DELETE)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length > 2) {
            sender.sendMessage(usage);
            return false;
        }
        Hologram  hologram = hologramHandler.getHologramByName(args[1]);

        if (hologram == null) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("HologramDoesNotExist")));
            return false;
        }
        hologramHandler.deleteHologram(hologram);
        sender.sendMessage(ColorUtils.translateMessage(config.getString("HologramDeleted").replace("{NAME}", hologram.getName())));

        return true;
    }
    private boolean move(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("OnlyPlayers")));
            return false;
        }
        if (!sender.hasPermission(Permissions.HOLOGRAM_COMMAND_MOVE)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length > 2) {
            sender.sendMessage(usage);
            return false;
        }
        Hologram  hologram = hologramHandler.getHologramByName(args[1]);

        if (hologram == null) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("HologramDoesNotExist")));
            return false;
        }
        hologramHandler.move(hologram, ((Player) sender).getLocation());
        sender.sendMessage(ColorUtils.translateMessage(config.getString("HologramMoved").replace("{NAME}", hologram.getName())));

        return true;
    }
    private boolean list(CommandSender sender, String[] args) {
        if (!sender.hasPermission(Permissions.HOLOGRAM_COMMAND_LIST)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length > 2) {
            sender.sendMessage(usage);
            return false;
        }
        int page = 0;

        try {
            page = Integer.parseInt(args[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (page <= 0) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("InvalidPage")));
            return false;
        }
        List<Hologram> holograms = hologramHandler.getHolograms();
        List<Hologram> paginated = CollectionUtils.paginateList(holograms, page, 5);

        if (paginated.isEmpty()) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("InvalidPage")));
            return false;
        }
        String format = ColorUtils.translateMessage(config.getString("HologramListFormat"));

        List<String> toString = paginated.stream().map(element -> format.replace("{HOLOGRAM}", element.getName())).collect(Collectors.toList());
        String combined = CollectionUtils.combineCollection(toString);

        List<String> hologramList = config.getStringList("HologramList");
        String s = ColorUtils.translateMessage(CollectionUtils.combineCollection(hologramList)).replace("{HOLOGRAMS}", combined);

        sender.sendMessage(s.replace("{PAGE}", args[1]));

        return true;
    }
    private boolean addLine(CommandSender sender, String[] args) {
        if (!sender.hasPermission(Permissions.HOLOGRAM_COMMAND_ADDLINE)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length < 3) {
            sender.sendMessage(usage);
            return false;
        }
        Hologram  hologram = hologramHandler.getHologramByName(args[1]);

        if (hologram == null) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("HologramDoesNotExist")));
            return false;
        }
        String text = "";

        for (int i = 2; i < args.length; i++) {
            text += text.isEmpty() ? args[i] : " " + args[i];
        }
        hologramHandler.addLine(text, hologram);
        sender.sendMessage(ColorUtils.translateMessage(config.getString("LineAdded").replace("{NAME}", hologram.getName())));

        return true;
    }
    private boolean removeLine(CommandSender sender, String[] args) {
        if (!sender.hasPermission(Permissions.HOLOGRAM_COMMAND_REMOVELINE)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length < 3 || args.length > 3) {
            sender.sendMessage(usage);
            return false;
        }
        Hologram  hologram = hologramHandler.getHologramByName(args[1]);

        if (hologram == null) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("HologramDoesNotExist")));
            return false;
        }
        int line = 1;

        try {
            line = Integer.parseInt(args[2]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean d = hologramHandler.removeLine(line, hologram);

        if (!d) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("InvalidLine")));
            return false;
        }
        sender.sendMessage(ColorUtils.translateMessage(config.getString("LineRemoved").replace("{NAME}", hologram.getName())));

        return true;
    }
    private boolean setLine(CommandSender sender, String[] args) {
        if (!sender.hasPermission(Permissions.HOLOGRAM_COMMAND_SETLINE)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length < 4) {
            sender.sendMessage(usage);
            return false;
        }
        Hologram  hologram = hologramHandler.getHologramByName(args[1]);

        if (hologram == null) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("HologramDoesNotExist")));
            return false;
        }
        int line = 1;

        try {
            line = Integer.parseInt(args[2]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String text = "";

        for (int i = 3; i < args.length; i++) {
            text += text.isEmpty() ? args[i] : " " + args[i];
        }
        hologramHandler.setLine(line, hologram, text);
        sender.sendMessage(ColorUtils.translateMessage(config.getString("LineSet").replace("{LINE}", args[2]).replace("{NAME}", hologram.getName())));

        return true;
    }
}
